#!/bin/bash
#
# Author: Zach Washburn
# Date: 2016-05-11
# Purpose: Create all versions of zStock odin flashable tar.md5 and boot.img
# Usage: ./build_all_kernels.sh
#

# 1. Get cross compile toolchain
git clone https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/arm/arm-eabi-4.7

# 2. Export path for tools
export PATH=$PATH:$PWD/staging/mkbootimg_tools:$PWD/arm-eabi-4.7

# 3. Set Variables and create output dir
REL_NAME=p607t-3.4-zStock
mkdir output

# 4. Unpack kernel source
mkdir staging/Kernel && tar zxvf original_source/Kernel.tar.gz -C staging/Kernel

# 5. Build each version
cd versions
for V in $(ls -d */); do 
  cp -rfp ${D%%/}/* ../staging/Kernel
  cd ../staging/Kernel
  ./build_kernel.sh
  cd .. && ./build_flashable_kernel.sh $REL_NAME-${V%%/}
  mv output/* ../output
done
