#!/bin/bash
#
# Author: Zach Washburn
# Date: 2016-05-12
# Purpose: Create odin flashable tar.md5 and boot.img from zImage
# Usage: ./build_flashable_kernel.sh [path_to_zImage] [output_filename] [OPTIONS]
# Options: 
#    -w   only produce .tar.gz
#

# 1. Check for filename arguments
if [ -z "$1" ] || [ -z "$2" ]
  then
    echo
    echo 'ERROR: Missing parameters'
    echo
    echo '	Usage: ./build_flashable_kernel.sh [path_to_zImage] [output_filename]'
    echo
    exit 1
fi

# 2. Check for existence of zImage file
if [ ! -e $1 ]
  then
    echo
    echo 'ERROR: zImage file not found'
    echo
    exit 1
fi

# 3. Check for mkbootimg_tools
if [ ! -e ./mkbootimg_tools ]
  then
    echo
    echo 'WARNING: mkbootimg_tools not found.  Fetching mkbootimg_tools from git repo...'
    echo
    git clone https://github.com/xiaolu/mkbootimg_tools.git
fi

# 4. copy kernel image to staging
cp $1 ./imgfiles/kernel

# 5. pack boot image
mkdir output
./mkbootimg_tools/mkboot imgfiles output/boot.img

# 6. pack ODIN flashable kernel
cd output &&  \
tar -H ustar -c boot.img > $2.tar &&  \
md5sum -t $2.tar >> $2.tar && \
mv $2.tar $2.tar.md5
if [[ $3 != *"w"* ]]
  then
    mv boot.img $2.boot.img
else
  rm boot.img
fi

# 7. exit message
echo 
echo '=============================================================='
echo 'ODIN flashable kernel created successfully in output directory'
echo '=============================================================='
echo 'Files created:'
echo
echo 'output/'$2'.tar.md5'
if [[ $3 != *"w"* ]]
  then
    echo 'output/'$2'.boot.img'
fi
echo '--------------------------------------------------------------'
echo